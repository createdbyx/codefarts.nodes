namespace Codefarts.Nodes
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Provides a model for generic node hierarchies.
    /// </summary>
    public class Node : INotifyPropertyChanged
    {
        /// <summary>
        /// The backing field for the <see cref="Parent"/> property.
        /// </summary>
        private Node parent;

        /// <summary>
        /// The backing field for the <see cref="Properties"/> property.
        /// </summary>
        private IDictionary<string, object> properties;

        /// <summary>
        /// The backing field for the <see cref="Children"/> property.
        /// </summary>
        private IList<Node> children;

        /// <summary>
        /// The backing field for the <see cref="Name"/> property.
        /// </summary>
        private string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="Node"/> class.
        /// </summary>
        public Node()
        {
            this.Children = new List<Node>();
            this.Properties = new Dictionary<string, object>();
        }

        /// <summary>
        ///  Initializes a new instance of the <see cref="Node"/> class.
        /// </summary>
        /// <param name="name">The value to assign to the <see cref="Name"/> property.</param>
        public Node(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Gets or sets the name associated with the node element.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (this.name != value)
                {
                    this.name = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the node children.
        /// </summary>
        public IList<Node> Children
        {
            get
            {
                return this.children;
            }

            set
            {
                if (!ReferenceEquals(this.children, value))
                {
                    this.children = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the node element properties.
        /// </summary>
        public IDictionary<string, object> Properties
        {
            get
            {
                return this.properties;
            }

            set
            {
                if (this.properties != value)
                {
                    this.properties = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        public Node Parent
        {
            get
            {
                return this.parent;
            }

            set
            {
                if (this.parent != value)
                {
                    this.parent = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Raised when a property value has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises the <see cref="PropertyChanged"/> event.
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}